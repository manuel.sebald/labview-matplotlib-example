"""
Basic test function for LabVIEW.
"""
def hello_world(greating: str) -> str:
    return "Hello {}!".format(greating)
