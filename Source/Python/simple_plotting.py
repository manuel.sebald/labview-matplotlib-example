"""
Matplotlib examples for LabVIEW.
"""

import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

def simple_plot():
    # Generate sample data.
    x = np.linspace(-10, 10, 100)
    y = np.sin(x)

    # Prepare the figure and canvas.
    fig = Figure(figsize=(6, 4), dpi=100)
    canvas = FigureCanvasAgg(fig)

    # Create a plot displaying the sample data.
    ax = fig.add_subplot(111)
    ax.plot(x, y, label="Sinus")
    ax.set_xlabel("X-Achse")
    ax.set_ylabel("Y-Achse")
    ax.grid(True)
    ax.legend()
    
    # Retrieve the rendered bitmap and return it to LabVIEW.
    canvas.draw()
    buf = canvas.buffer_rgba()
    return np.asarray(buf)
