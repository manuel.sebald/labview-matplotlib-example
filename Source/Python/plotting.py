"""
Matplotlib examples for LabVIEW.
"""

import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

# Prepare the figure-, canvas- and axes-objects  globaly.
fig = Figure(figsize=(6, 4), dpi=100)
canvas = FigureCanvasAgg(fig)
ax = fig.add_subplot(111)

def plot(x, y, plot_props):
    # Unpack the properties:
    title, line_label, x_axis, y_axis, grid_on, legend_on = plot_props

    # Create the plot:
    ax.set_title(title)
    ax.plot(x, y, label=line_label)
    ax.set_xlabel(x_axis)
    ax.set_ylabel(y_axis)
    ax.grid(grid_on)
    if legend_on:
        ax.legend()
    else:
        if ax.get_legend(): ax.get_legend().remove()
    
    
    # Retrieve the rendered bitmap and return it to LabVIEW.
    canvas.draw()
    buf = canvas.buffer_rgba()
    return np.asarray(buf)

def to_pdf(fn):
    fig.savefig(fn, format="pdf")

def set_properties(plot_props):
    # Unpack the properties:
    title, line_label, x_axis, y_axis, grid_on, legend_on = plot_props

    # Set the axes properties:
    ax.set_title(title)
    ax.set_xlabel(x_axis)
    ax.set_ylabel(y_axis)
    ax.grid(grid_on)
    if legend_on:
        ax.legend()
    else:
        if ax.get_legend(): ax.get_legend().remove()

    # Retrieve the rendered bitmap and return it to LabVIEW.
    canvas.draw()
    buf = canvas.buffer_rgba()
    return np.asarray(buf)
