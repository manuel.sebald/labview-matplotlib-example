my_class = None

class My_Class():
    def __init__(self, x):
        self.x = x
    
    def get_x(self):
        return self.x

def init_my_class(x):
    global my_class
    my_class = My_Class(x)

def get_x_my_class():
    return my_class.get_x()
    