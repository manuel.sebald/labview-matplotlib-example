# Labview Matplotlib Example

An example project to demonstrate the creation of "publishing quality" graphs with Matplotlib and LabVIEW over the Python interface. Created for a presentation at the [WueLUG11](https://forums.ni.com/t5/WUELUG-W%C3%BCrzburg-LabVIEW-User/WUELUG11-Virtuelles-Treffen-im-Oktober-2020/gpm-p/4087929).


## Prerequisites

- LabvVIEW 2020 or newer
- Python 3.6 or newer

**Warning:** Don't use the Python distribution _Anaconda_ with LabVIEW, it will not work.


The following Pyhton packages must be installed with `pip``:

- numpy
- matplotlib
- notebook (optional)


## Useful resources

- https://python.org
- https://jupyther.org
- https://matplotlib.org

