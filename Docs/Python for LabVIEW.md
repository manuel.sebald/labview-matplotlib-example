# How to install Python for LabVIEW

### Required packages

  - numpy
  - matplotlib

### Recommended packages

  - notebook
  - spyder
  